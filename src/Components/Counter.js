import React,{Component} from 'react'

class Counter extends Component{
  constructor(props){
    super(props)
    this.state={
      value:props.initialState
    }

    this.onIncrement=this.onIncrement.bind(this)
    this.onDecrement=this.onDecrement.bind(this)
  }

  onIncrement(){
    var newvalue=this.state.value+1
    this.setState({value:newvalue})
    this.props.onUpdate(this.state.value,this.state.value-1)
  }

  onDecrement(){
    var newvalue=this.state.value-1
    this.setState({value:newvalue})
    this.props.onUpdate(this.state.value,this.state.value+1)
  }

  render(){
    return (
      <div>
        <button onClick={()=>this.onDecrement()}>-</button> {this.state.value}<button onClick={()=>this.onIncrement()}>+</button>
      </div>
    )
  }
}

export default Counter;
