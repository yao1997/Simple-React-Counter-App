import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import ControlPanel from './Components/ControlPanel';
import {store,INC,DEC} from './Store.js'

ReactDOM.render(
	<ControlPanel/>,document.getElementById('root')
);
